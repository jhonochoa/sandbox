import freenect
import cv2
import numpy as np


#funcion para optener imagen de la camara(RGB)
def get_video():
    array,_ = freenect.sync_get_video()
    array = cv2.cvtColor(array,cv2.COLOR_RGB2BGR)
    #print array
    #print array.size
    #print hola
    return array


#function para optner la imagen en profundidad
def get_depth():
    array,_ = freenect.sync_get_depth()
    array = array.astype(np.uint8)

    arrayr = array.astype(np.uint8)
    arrayg = array.astype(np.uint8)
    arrayb = array.astype(np.uint8)
    maximo=float(np.amax(array))
    minimo=float(np.amin(array))
    array_aux=array.astype(float)

    for i in range(479):
        for j in range(639):
            array_aux[i][j]=(array_aux[i][j]-minimo)/(maximo-minimo)
            a=(1-array_aux[i][j])/0.25
            X=int(a)
            Y=int(255*(a-X))
            if X==0:
                arrayr[i][j]=255
                arrayg[i][j]=Y
                arrayb[i][j]=0
            if X==1:
                arrayg[i][j]=255
                arrayg[i][j]=255
                arrayb[i][j]=0
            if X==2:
                arrayg[i][j]=0
                arrayg[i][j]=255
                arrayb[i][j]=Y
            if X==3:
                arrayg[i][j]=0
                arrayg[i][j]=255
                arrayb[i][j]=255
            if X==4:
                arrayg[i][j]=0
                arrayg[i][j]=0
                arrayb[i][j]=255
    arraybgr= cv2.merge((arrayb,arrayg,arrayr))
    return arraybgr

if __name__ == "__main__":
    while 1:
        #frame de la camara(RGB)
        frame = get_video()
        #frame del sensor de profundidad
        depth = get_depth()
        #imagen de la camara en frame
        cv2.imshow('RGB image',frame)
        #imagen del sensor de profundida en frame
        cv2.imshow('Depth image',depth)

        # acabar el programa con "esc"
        k = cv2.waitKey(5) & 0xFF
        if k == 27:
            break
    cv2.destroyAllWindows()
